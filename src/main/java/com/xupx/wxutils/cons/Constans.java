package com.xupx.wxutils.cons;

/**
 * 微信基础配置
 * @author xupx
 */
public class Constans {
    /**
     * 主动推送信息接口
     */
    public static final String PUBLISH_MSG_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";

    /**
     * 获取关注用户信息接口
     */
    public static final String USERINFO_URL ="https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

    /**
     * 获取token接口APP_ID和SECRET替换成微信提供的
     */
    public static final String TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APP_ID&secret=SECRET";

    /**
     * 获取openId接口APP_ID和SECRET替换成微信提供的
     */
    public static final String OPEN_ID_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=APP_ID0&secret=SECRET&js_code=CODE&grant_type=authorization_code";

    /**
     * 获取TICKET，ACCESS_TOKEN替换成缓存中的或者自己获取下
     */
    public static final String TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";

    /**
     * 获取授权用户的基本信息
     */
    public static final String AUTHORIZATION_USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPEN_ID&lang=zh_CN";

}
