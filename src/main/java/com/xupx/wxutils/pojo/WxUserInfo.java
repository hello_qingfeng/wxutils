package com.xupx.wxutils.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息返回实体类
 * @author xupx
 */
@Data
public class WxUserInfo implements Serializable {
    private String nickname;
    private String headimgurl;
}
