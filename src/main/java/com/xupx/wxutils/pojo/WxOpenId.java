package com.xupx.wxutils.pojo;

import lombok.Data;

/**
 * 获取openId实体类
 * @author xupx
 */
@Data
public class WxOpenId extends WxAccessToken{
    private String openid;
}
