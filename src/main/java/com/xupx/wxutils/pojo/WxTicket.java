package com.xupx.wxutils.pojo;

import lombok.Data;

/**
 * ticket返回实体类
 * @author xupx
 */
@Data
public class WxTicket {
    private String ticket;
}
