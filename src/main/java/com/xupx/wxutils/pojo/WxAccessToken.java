package com.xupx.wxutils.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 获取access_token返回实体类
 * @author xupx
 */
@Data
public class WxAccessToken implements Serializable {

    private String access_token;
}
